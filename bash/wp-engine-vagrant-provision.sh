#!/bin/bash

# This script will clone the WP Engine git repo, as well as pull in
# any required files for WP Core, uploads, and the Vagrant environment

# console colors (because it's pretty!)
GREEN='\033[0;32m'
PURPLE='\033[0;35m'
RED='\033[0;31m'
CLEAR='\033[0m'

# directory names
TARGET_DIR='mclarens.com'
SOURCE_DIR='mclarens-full-tmp'
# git repos
MAIN_REPO='git@git.neboagency.com:nebo/mclarens.git'
SUPP_REPO='git@git.neboagency.com:nebo/mclarens-supplement.git'
WP_ENGINE_DEV_REPO='git@git.wpengine.com:production/nebomclarens.git'
WP_ENGINE_PROD_REPO='git@git.wpengine.com:production/mclarensnew.git'

DEV_URL='http://wpvagrant.test/'

# if the TARGET_DIR directory already exists, let's assume this has already
# been cloned and the environment set up
if [ ! -d $TARGET_DIR ]; then
  # clone the clean WP Engine repo
  echo -e "\n${GREEN}Cloning base repo${CLEAR}\n"
  git clone $MAIN_REPO $TARGET_DIR

  # clone the full repo with all content and Vagrant
  echo -e "\n${GREEN}Cloning Vagrant supplement repo${CLEAR}\n"
  git clone $SUPP_REPO $SOURCE_DIR

  # copy all required WP files to run vagrant
  echo -e "\n${GREEN}Copying WP Core files, uploads, and Vagrant config${CLEAR}\n"
  cp -R $SOURCE_DIR/manifests $TARGET_DIR/manifests
  cp -R $SOURCE_DIR/wp-admin $TARGET_DIR/wp-admin
  cp -R $SOURCE_DIR/wp-includes $TARGET_DIR/wp-includes
  cp -R $SOURCE_DIR/wp-content/uploads $TARGET_DIR/wp-content/uploads
  cp -R $SOURCE_DIR/wp-vagrant $TARGET_DIR/wp-vagrant
  cp $SOURCE_DIR/index.php $TARGET_DIR/index.php
  cp $SOURCE_DIR/Vagrantfile $TARGET_DIR/Vagrantfile
  cp $SOURCE_DIR/wp-activate.php $TARGET_DIR/wp-activate.php
  cp $SOURCE_DIR/wp-blog-header.php $TARGET_DIR/wp-blog-header.php
  cp $SOURCE_DIR/wp-comments-post.php $TARGET_DIR/wp-comments-post.php
  cp $SOURCE_DIR/wp-config.php $TARGET_DIR/wp-config.php
  cp $SOURCE_DIR/wp-cron.php $TARGET_DIR/wp-cron.php
  cp $SOURCE_DIR/wp-links-opml.php $TARGET_DIR/wp-links-opml.php
  cp $SOURCE_DIR/wp-load.php $TARGET_DIR/wp-load.php
  cp $SOURCE_DIR/wp-login.php $TARGET_DIR/wp-login.php
  cp $SOURCE_DIR/wp-mail.php $TARGET_DIR/wp-mail.php
  cp $SOURCE_DIR/wp-settings.php $TARGET_DIR/wp-settings.php
  cp $SOURCE_DIR/wp-signup.php $TARGET_DIR/wp-signup.php
  cp $SOURCE_DIR/wp-trackback.php $TARGET_DIR/wp-trackback.php
  cp $SOURCE_DIR/xmlrpc.php $TARGET_DIR/xmlrpc.php

  # get rid of what we no longer need
  echo -e "${GREEN}Removing temporary Vagrant supplemental files${CLEAR}\n"
  rm -rf $SOURCE_DIR

  # setup the correct git remotes for WP Engine deployment
  echo -e "\n${GREEN}Setting git remotes for dev/GitLab${CLEAR}\n"
  echo -e "${GREEN}To push to WP Engine development:\n\t${PURPLE}git push development master${CLEAR}\n"
  echo -e "${GREEN}To push to WP Engine production:\n\t${PURPLE}git push production master${CLEAR}\n"

  cd $TARGET_DIR
  git remote add development $WP_ENGINE_DEV_REPO
  git remote add production $WP_ENGINE_PROD_REPO

  echo -e "${GREEN}Environment setup completed!\n\n${PURPLE}cd${CLEAR}${GREEN} to ${PURPLE}${TARGET_DIR}${CLEAR}${GREEN} directory and run ${PURPLE}vagrant up${CLEAR}${GREEN} to view the site\nat ${PURPLE}${DEV_URL}${CLEAR}\n"
else
  echo -e "${RED}Target repository already exists. Exiting with no action.${CLEAR}"
fi
