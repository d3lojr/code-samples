#!/bin/bash

# Define a timestamp function
timestamp() {
  date +"%s"
}

PROD_IP='0.0.0.0'
PROD_USER='root'
PROD_PW='****'
PROD_DB='database_production'
PROD_DB_USER='rails'
PROD_DB_PW='****'

echo "Grabbing production database."

expect <<END
  spawn ssh $PROD_USER@$PROD_IP "pg_dump -U ${PROD_DB_USER} '${PROD_DB}' > redacted_tmp.sql"
  expect "password"
  send "${PROD_PW}\r"
  expect "Password"
  send "${PROD_DB_PW}\r"
END

# Force a pause here so the DB file is complete.
sleep 3s

expect <<END
  spawn scp $PROD_USER@$PROD_IP:~/redacted_tmp.sql .
  expect "password"
  send "${PROD_PW}\r"
  expect eof
END

# Force a pause here so the DB file is complete.
sleep 3s

if [[ $(psql -lqt | cut -d \| -f 1 | grep redacted-campaigns) ]]; then
  echo "Database already exists. Backing up current database."
  pg_dump -U postgres "redacted-campaigns" > "redacted-local_$(timestamp).sql"
  rake db:drop db:create
else
  echo "Database does not exist. Creating database."
  rake db:create
fi

echo "Importing production database."

psql -U postgres "redacted-campaigns" < redacted_tmp.sql

echo "Updating host file. You should be prompted for your OSX password."

if [ -n "$(grep campaigns.redacted /etc/hosts)" ]
then
    echo "campaigns.redacted already exists : $(grep campaigns.redacted /etc/hosts)"
else
    HOSTS_LINE="127.0.0.1\tcampaigns.redacted"
    echo "Adding campaigns.redacted to your hosts file.";
    sudo -- sh -c -e "echo '$HOSTS_LINE' >> /etc/hosts";

    if [ -n "$(grep campaigns.redacted /etc/hosts)" ]
        then
            echo "campaigns.redacted was added succesfully \n $(grep campaigns.redacted /etc/hosts)";
        else
            echo "Failed to Add campaigns.redacted, Try again!";
    fi
fi

if [ -n "$(grep hacks.redacted /etc/hosts)" ]
then
    echo "hacks.redacted already exists : $(grep hacks.redacted /etc/hosts)"
else
    HOSTS_LINE="127.0.0.1\thacks.redacted"
    echo "Adding hacks.redacted to your hosts file.";
    sudo -- sh -c -e "echo '$HOSTS_LINE' >> /etc/hosts";

    if [ -n "$(grep hacks.redacted /etc/hosts)" ]
        then
            echo "hacks.redacted was added succesfully \n $(grep hacks.redacted /etc/hosts)";
        else
            echo "Failed to Add hacks.redacted, Try again!";
    fi
fi

echo "Removing local copy of temporary production database."

rm redacted_tmp.sql

echo "Removing temporary production database from server."

expect <<END
  spawn ssh $PROD_USER@$PROD_IP "rm ~/redacted_tmp.sql"
  expect "password"
  send "${PROD_PW}\r"
  expect eof
END

echo "Updating local domain variables."

rails runner "Sites::Site.find_by(domain: 'campaigns.redacted.com').update(domain: 'campaigns.redacted')"
rails runner "Sites::Site.find_by(domain: 'hacks.redacted.com').update(domain: 'hacks.redacted')"

echo "You should now be able to navigate to http://campaigns.redacted:3000/move-1 in your browser."

foreman start
