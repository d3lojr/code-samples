class Members::Company < ActiveRecord::Base
  
  # ... Code removed at request of client

  def self.import(file, level)
    require 'roo'
    spreadsheet = Roo::CSV.new(file.path)
    notices = []

    header = spreadsheet.row(1).collect{|x| x.strip || x }
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      if row["Company"].present? && row["Address 1"].present? && row["City"].present? && row["State"].present? && row["Zip"].present?
        company = Members::Company.find_or_create_by(title: row["Company"])
        company[:address1] = row["Address 1"]
        company[:city]     = row["City"]
        company[:state]    = row["State"]
        company[:zip]      = row["Zip"]

        # optional category
        unless row["Industry"].nil? || row["Industry"].strip.blank?
          category = Members::Category.find_or_create_by(title: row["Industry"].strip)
          company.company_categories << category unless company.company_categories.include?(category)
        end
        
        # optional columns
        company[:address2] = row["Address 2"] if !row["Address 2"].blank?
        company[:website] = row["Website"] if !row["Website"].blank?
        company[:phone] = row["Phone"] if !row["Phone"].blank?

        # logo
        unless row["Logo"].blank?
          dir = ::Assets::Directory.find_or_create_by(parent_id: 1, dir_name: 'logos')
          filename = row['Logo'].gsub(/^.*(\\|\/)/, '').gsub(/[^0-9A-Za-z.\-]/, '_').downcase
          asset = ::Assets::Resource.find_or_create_by(parent_id: dir.id, resource_file_name: filename)
          if asset.new_record? || asset.resource_file_size.nil?
            if File.file?("public/member_logos/#{row['Logo']}")
              begin
                FileUtils.copy("public/member_logos/#{row['Logo']}", dir.path + filename)
                asset.resource = File.open(dir.path + filename)
                asset.caption = File.basename(row['Logo'], ".*").titleize
                file.close
                asset.save
              rescue => e
                notices << e.message
              end
            else
              notices << "[#{i}] Could not locate file: '#{row['Logo']}'"
            end
          end
          company.logo = asset.id
        end

        # optional representative
        if !row["Email"].blank? || !row["FirstName"].blank? || !row["LastName"].blank?
          unless row['Email'].blank?
            representative = Members::Representative.find_or_create_by(email: row["Email"])
          else
            representative = Members::Representative.new()
          end
          representative.primary = true
          representative.first_name = row["FirstName"] unless row["FirstName"].blank?
          representative.last_name = row["LastName"] unless row["LastName"].blank?
          representative.job = row["Job"] unless row["Job"].blank?
          representative.phone = row["Assistant Phone"] unless row["Assistant Phone"].blank?
          representative.company = company
          representative.save
        end

        if !level.nil?
          company.member_level = level.to_i
        end

        company.save
      else
        notices << "[#{i}] Required columns missing"
      end
    end

    notices
  end
end

