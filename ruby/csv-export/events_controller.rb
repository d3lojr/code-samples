class Cms::Events::EventsController < Cms::BaseController

  # ... Code removed at request of client

  def export_attendees
    @event ||= ::Events::Event.find(params[:event_id])

    respond_to do |format|
      format.html
      format.csv { render text: @event.export_attendees }
    end
  end

  def export_registrations
    @event ||= ::Events::Event.find(params[:event_id])

    respond_to do |format|
      format.html
      format.csv { render text: @event.export_registrations }
    end
  end

  def event
    @event ||= ::Events::Event.find(params[:id])
  end
end

