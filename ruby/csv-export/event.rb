class Events::Event < ActiveRecord::Base
  scope :public_events, -> { where(is_private: false) }
  scope :private_events, -> { where(is_private: true) }

  before_create :generate_private_token

  # ... Code removed at request of the client

  def start_time
    start.strftime('%m/%d/%Y %l:%M %p') unless start.blank?
  end

  def end_time
    self.end.strftime('%m/%d/%Y %l:%M %p') unless self.end.blank?
  end

  def contact
    ::People::Person.find_by(id: self[:contact_person])
  end

  def featured_page
    ::Pages::Page.find_by(id: self[:featured_page_id])
  end

  def full_address
    [ address, address_2, city, state, zip ].join(', ')
  end

  def export_attendees
    # TODO: Add Ticket Type/Ticket ID
    # in order to group attendees by table

    require 'csv'
    rows = []
    rows << ["#{title} Attendees, downloaded on #{Time.now.strftime('%m/%d/%Y %l:%M %p')}"]
    rows << ['Registration ID','Registration Date','Attendee ID','Company Name','First Name','Last Name','Email','Attended']

    self.attendees.where("deleted IS NULL OR deleted = ?", false).each do |a|
      unless a.ticket.registration.refunded?
        rows << [a.ticket.registration_id, a.ticket.registration.date, a.id, a.company_name, a.first_name, a.last_name, a.email, a.attended]
      end
    end

    CSV.generate do |csv|
      rows.each do |r|
        csv << r
      end
    end
  end

  def export_registrations
    require 'csv'
    rows = []
    rows << ["#{title} #{start} Registrations, downloaded on #{Time.now.strftime('%m/%d/%Y %l:%M %p')}"]
    rows << ['ID','Registration Date','Company','Bill Email','User','Total','Tickets','Attendees']

    self.registrations.each do |r|
      unless (r.refunded? || (r.attendees.where("deleted IS NULL OR deleted = ?", false).size == 0))
        row = [r.id, r.date, r.company_name, r.bill_email]
        if r.member_id.nil?
          row << 'N/A'
        else
          row << r.member.first_name.to_s + ' ' + r.member.last_name.to_s
        end
        row << r.total
        row << r.tickets.size
        row << r.attendees.where("deleted IS NULL OR deleted = ?", false).size
        rows << row
      end

      unless r.attendees.where("deleted IS NULL OR deleted = ?", false).blank?
        r.attendees.each do |a|
          unless a.ticket.registration.refunded?
            rows << [a.ticket_id, a.ticket.ticket_type, a.company_name, a.email, (a.first_name.to_s + " " + a.last_name.to_s), a.attended]
          end
        end
      end
    end

    CSV.generate do |csv|
      rows.each do |r|
        csv << r
      end
    end
  end

  def ical_link
    "/events/" + self.filename + "/ical"
  end

  protected

  def generate_private_token
    if self.respond_to?('private_token')
      self.private_token = loop do
        random_token = SecureRandom.urlsafe_base64(4)
        break random_token unless self.class.exists?(private_token: random_token)
      end
    end
  end
end

