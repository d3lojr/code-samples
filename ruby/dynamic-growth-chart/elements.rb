module Elements
  def self.load_data(site_id, content, page, options = {})
    unless content.nil?
      graph = content.settings
      numbers = []
      labels = []
      values = []

      (2010..2014).each do |i|
      	unless graph["value_#{i}"].blank?
      		numbers << graph["value_#{i}"].gsub(/[^0-9.]/, '').to_f
      		if graph["label_#{i}"].blank?
      			labels << i
      		else
      			labels << graph["label_#{i}"]
      		end
      	end
      end

      numbers.each_with_index do |v, i|
        values << {value: v, label: labels[i], height: ((((1-((numbers.max-v)/(numbers.max-numbers.min)))*0.7950)+0.2)*100), coord: ((numbers.max-v)/(numbers.max-numbers.min)*144) }
      end

      # Lowest number should have 20% height on LI
      # Highest number should be 100% height on LI
      # Lowest number should be Y coord of 144
      # Highest number should be Y coord of 0

      {
      	heading: graph['heading'], content: graph['content'], link_text: graph['link_text'], link: graph['link'],
      	graph_heading: graph['graph_heading'], footer_content: graph['footer_content'],
      	values: values
      }
    else
      {
        heading: 'Making Business Boom', content: '<p>&nbsp;</p>'.html_safe,
        link_text: 'View Details', link: '/resources',
        graph_heading: 'Gross Domestic Product (GDP) in Billions of U.S. Dollars',
        footer_content: '<p>Source:</p>'.html_safe,
        values: [
          {:value=>277.0, :label=>"2010", :height=>20.0, :coord=>144.0},
          {:value=>286.0, :label=>"2011", :height=>35.22340425531916, :coord=>116.42553191489361},
          {:value=>296.0, :label=>"2012", :height=>52.13829787234043, :coord=>85.7872340425532},
          {:value=>309.0, :label=>"2013", :height=>74.1276595744681, :coord=>45.95744680851064},
          {:value=>324.0, :label=>"2014", :height=>99.50000000000001, :coord=>0.0}
        ]
      }
    end
  end
end