class RouterController < ApplicationController

  # Code has been removed by request of client
  # ...

  private
    def find_sister_page(requested_route, to_site_obj = ::Sites::Site.find(1))
      if requested_route[:page_id].present?
        requested_page = Pages::Page.find(requested_route[:page_id])
        requested_route[:url] = File.join(requested_route[:url], "").gsub!(requested_page.site.url, to_site_obj.url)
        [Router.instance.get_routes, Router.instance.get_preview_routes].each do |route_set|
          route_set.each do |route|
            if(requested_route[:url] == route[:url])
              return route[:url]
            end
          end
        end
      end

      return to_site_obj.url
    end

    def geolocate(route)
      unless session[:geo_redirected].present?
        @force_picker = true
        location = Pointpin.locate(request.ip)

        if location.present?
          continent_codes = {
            "AF" => "Africa",
            "NA" => "North America",
            "OC" => "Oceania",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "SA" => "South America"
          }

          # Adding "the" before some country names
          # Taken from: https://www.grammar-quizzes.com/article4c.html
          country_replacements = {
            "Bahamas"                       => "the Bahamas",
            "Comoros"                       => "the Comoros",
            "Marshall Islands"              => "the Marshall Islands",
            "Seychelles"                    => "the Seychelles",
            "Gambia"                        => "the Gambia",
            "United States"                 => "the United States",
            "Congo"                         => "the Republic of the Congo",
            "Democratic Republic of Congo"  => "the Democratic Republic of the Congo",
            "Dominican Republic"            => "the Dominican Republic",
            "United Arab Emirates"          => "the United Arab Emirates",
            "United Kingdom"                => "the United Kingdom"
          }

          @user_continent = continent_codes[location['continent_code']]

          if location["country_code"].present?
            @geo_site = Sites::Site.find_by("country_codes ILIKE ?", "%"+location["country_code"]+"%")

            if @geo_site.present?
              unless @page.site.url == @geo_site.url
                # They're going to a site that doesn't match their region
                @user_country = location['country_name']
                country_replacements.each do |replacement|
                  @user_country = replacement[1] if @user_country.downcase == replacement[0].downcase
                end

                if @element.present?
                  # detail page. see if an element with the same class/url exists
                  new_element = @element.class.find_by(site_id: @geo_site.id, filename: @element.filename)
                  if new_element.present?
                    @geo_site.url = new_element.url
                  end
                else
                  # not a detail page. see if a page with the same url exists
                  @geo_site.url = find_sister_page(route, @geo_site)
                end
              else
                # They're going to the correct site already
                @force_picker = false
              end
            end
          else
            # we can't determine a country
          end
        end
      end
    end

end

