class Events::EventsController < ApplicationController
  def ical
    if params[:filename].present?
      if event = ::Events::Event.find_by(filename: params[:filename])
        require 'icalendar'

        cal = Icalendar::Calendar.new

        cal_event = Icalendar::Event.new
        cal_event.dtstart = event.start
        cal_event.dtend = event.end

        if event.location.present?
          cal_event.location = event.location
        end

        if event.address.present?
          unless Geocoder.search(event.full_address).blank?
            cal_event_geo = 'GEO:' + Geocoder.search(event.full_address).first.coordinates.join(';')
          end
        end

        cal_event.url = request.url.gsub('ical', '')
        cal_event.summary = event.title
        cal_event.description = ActionView::Base.full_sanitizer.sanitize(event.short_description.strip)

        if event.contact.present?
          cal_event.organizer = Icalendar::Values::CalAddress.new("mailto:#{event.contact.email}", cn: event.contact.name)
        end

        cal.add_event(cal_event)

        filename = event.title.parameterize

        send_data(cal.to_ical + cal_event_geo.to_s,:type => 'text/calendar', :disposition => "inline; filename=#{filename}.ics", :filename => "#{filename}.ics")
      end
    end
  end

  def community_event_page
    events = ::Events::CommunityEvent.where('(end_date IS NULL) OR (end_date IS NOT NULL and end_date > ?)', Time.now.in_time_zone).order(date: :asc).page(params[:page]).per(10)

    render partial: '/events/community_events/page', locals: { events: events }
  end

  def event_page
    per ||= params[:per]
    per ||= 10
    params[:category] = nil if params[:category] == "NaN"

    # if params[:category].present? && params[:level].present? && params[:level] == "0"
    #   events = ::Events::Event.joins(:categories).where(events_events_events_categories: { category_id: params[:category] }).order(spotlight: :desc, start: :asc).page(params[:page]).per(per)
    # els

    if params[:category].present? && params[:level].present?
      events = ::Events::Event.visible.public_events.joins(:categories).joins(:member_levels)
        .where('start > ?', Date.today)
        .where(events_events_events_categories: { category_id: params[:category] })
        .where(events_events_members_levels: { level_id: params[:level] })
        .order(spotlight: :desc, start: :asc).page(params[:page]).per(per)
    elsif params[:category].present?
      events = ::Events::Event.visible.public_events.joins(:categories).where('start > ?', Date.today)
      .where(events_events_events_categories: { category_id: params[:category] }).order(spotlight: :desc, start: :asc).page(params[:page]).per(per)
    # elsif params[:level].present? && params[:level] == "0"
    #   events = ::Events::Event.all.order(spotlight: :desc).order(start: :asc).order(spotlight: :desc, start: :asc).page(params[:page]).per(per)
    elsif params[:level].present?
      if params[:level] == 'council'
        events = ::Events::Event.visible.public_events.where('start > ?', Date.today)
          .order(spotlight: :desc).order(start: :asc).joins(:councils).distinct.page(params[:page]).per(per)
      else
        level = ::Members::Level.find_by(filename: params[:level])
        unless level.nil?
          events = ::Events::Event.visible.public_events.joins(:member_levels).where('start > ?', Date.today)
          .where(events_events_members_levels: { level_id: level.id }).order(spotlight: :desc, start: :asc).page(params[:page]).per(per)
        else
          events = ::Events::Event.visible.public_events.where('start > ?', Date.today).order(spotlight: :desc, start: :asc).page(params[:page]).per(per)
        end
      end
    else
      # events = ::Events::Event.joins(:member_levels).where(events_events_members_levels: { level_id: 1 }).order(spotlight: :desc, start: :asc).page(params[:page]).per(per)
      events = ::Events::Event.visible.public_events.where('start > ?', Date.today).order(spotlight: :desc, start: :asc).page(params[:page]).per(per)
    end

    render partial: '/events/events/page', locals: { events: events }
  end

  def attendees
    @page = ::Pages::Page.find_by_filename('event-registration')

    if params[:filename].present?
      @element = ::Events::Event.visible.find_by_filename(params[:filename])

      if @element.present?
        # Validate quantity fields
        total_tickets = 0
        subtotal = 0
        auth_required = false
        if params[:events_event]
          params[:events_event][:tickets_group].each do |j, ticket_group|
            ticket_group[:tickets_ticket_type].each do |k, ticket|
              ticket_obj = ::Tickets::TicketType.find(k)
              if ticket[:quantity].to_i > 0
                total_tickets += ticket[:quantity].to_i
                subtotal += ticket[:quantity].to_i * ticket_obj.price.to_d
                if ticket_obj.members_only?
                  auth_required = true
                end
              end
            end
          end
        else
          redirect_to @element.url and return
        end

        if auth_required
          unless current_member || (params.present? && params[:express].present?)
            if params.present? && params[:members_user] && !params[:members_user][:email].blank?
              user = ::Members::User.where('lower(email) = ?', params[:members_user][:email].downcase).first
              if user
                if user.password_digest.blank?
                  user.update(forgot_token: SecureRandom.urlsafe_base64)
                  render json: { redirect: members_reset_password_url(user.forgot_token) } and return
                elsif user.authenticate(params[:members_user][:password])
                  session[:member_id] = user.id
                  cookies[:member_remember_token] = user.remember_token
                  self.current_member = user
                end
              else
                if !user
                  if params[:ajax]
                    render json: { field: 'members_user_email', error: "Could not locate an account with this email." } and return
                  else
                    flash[:error] = "Could not locate an account with this email."
                    redirect_to @element.url and return
                  end
                else
                  if params[:ajax]
                    render json: { field: 'members_user_password', error: "This password is invalid." } and return
                  else
                    flash[:error] = "Invalid password."
                    redirect_to @element.url and return
                  end
                end
              end
            end
          end
        end

        if total_tickets < 1
          flash[:error] = 'Please select some tickets.'
          redirect_to @element.url and return
        end

        unless member_logged_in?
          if (auth_required == true && !params[:express].present?)
            flash[:error] = 'You selected a members only event.'
            redirect_to @element.url and return
          end
        end

        @registration = ::Events::Registration.new()
        @ticket = ::Tickets::Ticket.new()
        @attendee = ::Events::Attendee.new()

        render partial: 'attendees', layout: 'event_registration',
          :locals => {
            auth_required: auth_required,
            subtotal: subtotal,
            registration: @registration,
            attendee: @attendee
          }
      else
        redirect_to '/events' and return
      end
    else
      redirect_to '/events' and return
    end
  end

  def payment
    @page = ::Pages::Page.find_by_filename('event-registration')

    if params[:filename].present?
      @element = ::Events::Event.visible.find_by_filename(params[:filename])

      if @element.present?
        if params[:tickets_ticket].present?
          session[:tickets_ticket] = params[:tickets_ticket]
        end

        if session[:tickets_ticket].present?
          @subtotal = 0
          auth_required = false

          session[:tickets_ticket].each do |t|
            ticket_obj = ::Tickets::TicketType.find(t["ticket_type_id"].to_i)
            # @subtotal += ticket_obj.price.to_d unless (params[:events_registration].present? && params[:events_registration][:express] == "1")
            @subtotal += ticket_obj.price.to_d
            if ticket_obj.members_only?
              auth_required = true
            end
          end

          @registration = ::Events::Registration.new()

          render partial: 'payment', layout: 'event_registration', :locals => { registration: @registration, total: @subtotal, auth_required: auth_required }
        else
          flash[:notice] = 'Please select some tickets.'
          redirect_to @element.url and return
        end
      else
        redirect_to '/events' and return
      end
    else
      redirect_to '/events' and return
    end
  end

  def final_step
    @page = ::Pages::Page.find_by_filename('event-registration')

    if params[:filename].present?
      @element = ::Events::Event.find_by_filename(params[:filename])

      if @element.present?
        # All free events and express checkout should use Captcha
        if (params.has_key?("g-recaptcha-response") && params["g-recaptcha-response"].blank?)
          flash[:error] = "Please verify you are not a robot."
          if (params[:events_registration].present? && params[:events_registration][:express] == "1")
            redirect_to events_payment_path(filename: @element.filename, events_registration: {express: "1"}) and return
          else
            redirect_to events_payment_path(filename: @element.filename) and return
          end
        end

        if session[:tickets_ticket]
          @tickets = session[:tickets_ticket]
          total = 0
          auth_required = false

          @tickets.each do |t|
            ticket_obj = ::Tickets::TicketType.find(t["ticket_type_id"].to_i)
            total += ticket_obj.price.to_d
            if ticket_obj.members_only?
              auth_required = true
            end
          end

          @registration = ::Events::Registration.new(
            event_id: @element.id,
            date: Time.now,
            bill_email: "#{params[:billing_email]}"
          )

          if member_logged_in?
            @registration.company_name = current_member.company.title
            @registration.member_id = current_member.id
          else
            @registration.company_name = params[:company_name]
          end
          @registration.save

          # We can skip this for free events
          if total > 0
            # unless (params[:events_registration].present? && params[:events_registration][:express].present?)
              require 'stripe'

              # Set API Key to Secret Key
              Stripe.api_key = Rails.configuration.stripe[:secret_key]

              token = params[:stripeToken]

              # Create a charge: this will charge the user's card
              begin
                @charge = Stripe::Charge.create(
                  :amount => (total.to_d * 100).to_i, # Amount in cents
                  :currency => "usd",
                  :source => token,
                  :receipt_email => "#{params[:billing_email]}",
                  :description => "Registration #{@registration.id} for #{@element.title}",
                  :metadata => {
                    "registration_id" => "#{@registration.id}",
                    "event_id" => "#{@element.id}",
                    "company_name" => "#{@registration.company_name}"
                  }
                )
              rescue Stripe::CardError => e
                Stripe.api_key = Rails.configuration.stripe[:publishable_key]
                flash[:error] = e.json_body[:error][:message]
                redirect_to events_payment_path(filename: @element.filename) and return
              rescue Stripe::RateLimitError => e
                # Too many requests made to the API too quickly
                Stripe.api_key = Rails.configuration.stripe[:publishable_key]
                flash[:error] = e.json_body[:error][:message]
                redirect_to events_payment_path(filename: @element.filename) and return
              rescue Stripe::InvalidRequestError => e
                # Invalid parameters were supplied to Stripe's API
                Stripe.api_key = Rails.configuration.stripe[:publishable_key]
                flash[:error] = e.json_body[:error][:message]
                redirect_to events_payment_path(filename: @element.filename) and return
              rescue Stripe::AuthenticationError => e
                # Authentication with Stripe's API failed
                # (maybe you changed API keys recently)
                Stripe.api_key = Rails.configuration.stripe[:publishable_key]
                flash[:error] = e.json_body[:error][:message]
                redirect_to events_payment_path(filename: @element.filename) and return
              rescue Stripe::APIConnectionError => e
                # Network communication with Stripe failed
                Stripe.api_key = Rails.configuration.stripe[:publishable_key]
                flash[:error] = e.json_body[:error][:message]
                redirect_to events_payment_path(filename: @element.filename) and return
              rescue Stripe::StripeError => e
                # Display a very generic error to the user, and maybe send
                # yourself an email
                Stripe.api_key = Rails.configuration.stripe[:publishable_key]
                flash[:error] = e.json_body[:error][:message]
                redirect_to events_payment_path(filename: @element.filename) and return
              rescue => e
                # Something else happened, completely unrelated to Stripe
                Stripe.api_key = Rails.configuration.stripe[:publishable_key]
                flash[:error] = e.json_body[:error][:message]
                redirect_to events_payment_path(filename: @element.filename) and return
              end

              # Set API Key Back to Public Key
              Stripe.api_key = Rails.configuration.stripe[:publishable_key]

              if @charge.present?
                @registration.update(transaction_id: @charge.id)
              else
                flash[:error] = "We were unable to charge your card."
                @registration.destroy
                redirect_to events_attendees_path(filename: @element.filename) and return
              end
            # else
              # EXPRESS CHECKOUT
            # end
          else
            # FREE EVENT
          end

          @tickets.each do |t|
            ticket = ::Tickets::Ticket.new(
              registration_id: @registration.id,
              ticket_type_id: t["ticket_type_id"]
            )
            ticket.save

            t["events_attendee"].each do |a|
              attendee = ::Events::Attendee.new(
                ticket_id: ticket.id,
                first_name: a["first_name"].try(:titleize),
                last_name: a["last_name"].try(:titleize),
                email: a["email"].try(:downcase),
                company: a["company"].try(:titleize),
                attended: false
              )
              attendee.save
            end
          end

          require 'wicked_pdf'

          @event = @element

          pdf = WickedPdf.new.pdf_from_string(
            render_to_string( partial: "events/events/invoice.html.erb" ),
            :viewport_size  => '1280x1024',
            :margin         => { bottom: 0, top: 0, left: 0, right: 0 }
          )

          filename = Time.now.strftime('%Y-%m-%d_') + @registration.id.to_s
          save_path = Rails.root.join('public','invoices','events',"#{filename}.pdf")
          File.open(save_path, 'wb') do |file|
            file << pdf
          end

          @registration.update(invoice: filename)

          # Email User
          EventsMailer.new_registration(@registration, @charge).deliver

          # Newsletter Subscribe
          if params[:newsletter].present? && params[:newsletter] == '1'
            Events::Registration.delay.newsletter_subscribe(@registration.id)
          end

          # TODO: CLEAR OUT SESSION VARIABLES
          session.delete(:tickets_ticket)

          render partial: 'thank_you', layout: 'event_registration', :locals => { registration: @registration, charge: @charge }
        else
          redirect_to events_attendees_path(filename: @element.filename) and return
        end
      else
        redirect_to '/events' and return
      end
    else
      redirect_to '/events' and return
    end
  end
end
