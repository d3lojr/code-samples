class Api::ApiController < ApplicationController
  def readme
    render text: File.open(Rails.root + "doc/API_README.md", 'r').read
  end

  private
 
  def authenticate
    api_key = request.headers['X-Api-Key']

    key = ::Apis::Key.find_by_key(api_key)
    if key.nil?
      head status: :unauthorized
      return false
    end
  end
end