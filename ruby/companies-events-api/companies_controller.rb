class Api::CompaniesController < Api::ApiController
  respond_to :json

  before_action :authenticate

  def index
  	respond_with Members::Company.all.order(:title).as_json({
      :only => [:id, :title, :created_at, :updated_at]
    })
  end

  def categories
    respond_with Members::Category.all.order(:title).as_json({
      :only => [:id, :title, :subtitle]
    })
  end

  def levels
    respond_with Members::Level.where.not(title: 'Employee').order(:title).as_json({
      :only => [:id, :title, :created_at, :updated_at]
    })
  end

  def search
    if params[:query].present?
      companies = ::Members::Company.where("members_companies.title ILIKE ?", '%'+params[:query]+'%').order('LOWER(members_companies.title)')
      unless companies.blank?
        render json: companies.as_json({
          :except => [:member_level, :logo],
          :include => {:users => {
            :only => [:id, :first_name, :last_name, :email, :job]
          }, :representatives => {
            :except => [:company_id, :created_at, :updated_at]
          }, :company_categories => {
            :except => [:created_at, :updated_at]
          }}
        }) and return
      else
        render json: { status: :unprocessable_entity, error: "No matches found" } and return
      end
    else
      render json: { status: :unprocessable_entity, error: "Query string not found" } and return
    end
  end

  def show
    if company = Members::Company.find_by(id: params[:id])
      render json: company.as_json({
        :except => [:member_level],
        :include => {:users => {
          :only => [:id, :first_name, :last_name, :email, :job]
        }, :representatives => {
          :except => [:company_id, :created_at, :updated_at]
        }, :company_categories => {
          :except => [:created_at, :updated_at]
        }}
      }).merge({ level: ::Members::Level.find(company[:member_level]).title }) and return
    else
      render json: { status: :unprocessable_entity, error: "Company not found" } and return
    end
  end

  def update
    if params[:company].present? && (params[:company][:title].present? || params[:company][:id].present?)
      if params[:company][:id].present?
        company = ::Members::Company.find_by(id: params[:company][:id].to_i)
      else
        company = ::Members::Company.where('lower(title) = ?', params[:company][:title].downcase).first
        if company.nil?
          company = ::Members::Company.create(title: params[:company][:title])
        end
      end

      unless company.nil?
        company.update_attributes(company_params)

        if params[:company][:member_level].present?
          level = ::Members::Level.find_by(id: params[:company][:member_level])
          if level && level.title != 'Employee'
            company[:member_level] = params[:company][:member_level]
          elsif company[:member_level].nil?
            company[:member_level] = 1
          end
        elsif company[:member_level].nil?
          company[:member_level] = 1
        end
        company.save

        if params[:company][:categories].present? && params[:company][:categories].kind_of?(Array)
          company_categories = []
          params[:company][:categories].each do |new_cat|
            if new_cat[:id].present? && new_cat[:id].kind_of?(Integer)
              cat = ::Members::Category.find_by(id: new_cat[:id])
              company_categories << new_cat[:id] unless cat.nil?
            elsif new_cat[:title].present? && new_cat[:title].kind_of?(String)
              cat = ::Members::Category.where('lower(title) = ?', new_cat[:title].downcase).first
              company_categories << cat[:id] unless cat.nil?
            end
          end
          company.company_category_ids = company_categories
          company.save
        end

        render json: company.as_json({
          :except => [:member_level],
          :include => {:representatives => {
            :except => [:company_id]
          }}
        }).merge({ level: ::Members::Level.find(company[:member_level]).title }) and return
      else
        render json: { status: :unprocessable_entity, error: "Company not found" } and return
      end
    else
      render json: { status: :unprocessable_entity, error: "Company data not found" } and return
    end
  end

  def representatives
    if params[:representative].present?

      if params[:representative][:id].present?
        rep = ::Members::Representative.where(id: params[:representative][:id])
      elsif params[:representative][:email].present? && params[:representative][:company_id].present?
        rep = ::Members::Representative.where(email: params[:representative][:email], company_id: params[:representative][:company_id])
      elsif params[:representative][:email].present?
        rep = ::Members::Representative.where(email: params[:representative][:email])
      end

      if rep.nil? && params[:representative][:company_id].blank?
        render json: { status: :unprocessable_entity, error: "Missing required parameters" } and return
      elsif rep.blank?
        if params[:representative][:company_id].blank?
          render json: { status: :unprocessable_entity, error: "New representative must contain company_id" } and return
        else
          rep = ::Members::Representative.create(rep_params)
        end
      elsif rep.size == 1
        rep = rep.first
        if params[:representative][:delete].present? && params[:representative][:delete] == true
          rep.destroy
        else
          rep.update(rep_params)
        end
      elsif rep.size > 1
        render json: { status: :unprocessable_entity, error: "Multiple representatives found. Please specify an ID or company_id", matches: rep.as_json } and return
      end

      render json: rep.as_json and return
    else
      render json: { status: :unprocessable_entity, error: "Representative data not found" } and return
    end
  end

  def rep_params
    params.require(:representative).permit(:first_name, :last_name, :job, :email, :phone, :primary, :company_id)
  end

  def company_params
    params.require(:company).permit(:address1, :address2, :city, :state, :zip, :website, :phone, :employees, :international, :active)
  end
end
