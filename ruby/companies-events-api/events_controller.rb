class Api::EventsController < Api::ApiController
  respond_to :json

  before_action :authenticate

  def index
  	respond_with Events::Event.all.order(start: :asc).as_json({
      :only => [:id, :title, :start, :end]
    })
  end

  def show
    if event = Events::Event.find_by(id: params[:id])
      render json: event.as_json({
        :only => [:title, :start, :end],
        :include => {:registrations => {
          :only => [:id, :company_name, :bill_name, :bill_email, :created_at, :updated_at],
          :include => {:tickets => {
            :only => [],
            :include => {:attendees => {
              :only => [:first_name, :last_name, :email, :attended, :deleted]
            }}
          }}
        }}
      }).merge({ description: event[:description] })
    else
      render json: { status: :unprocessable_entity, error: "Event not found" }
    end
  end

  def registrations
    if params[:start].present?
      if Date.parse(params[:start])
        if params[:end].present? && Date.parse(params[:end])
          end_date = Date.parse(params[:end])
        else
          end_date = Time.now
        end

        registrations = Events::Registration.where(created_at: Date.parse(params[:start])..end_date)
        render json: registrations.as_json({
          :include => {:member => {
            :only => [:first_name, :last_name, :job, :email],
            :include => {:company => {
              :except => [:member_level, :logo]
            }}
          }, :event => {
            :only => [:title, :start, :end]
          }, :tickets => {
            :only => [],
            :methods => :ticket_type,
            :include => {:attendees => {
              :only => [:first_name, :last_name, :email, :attended, :deleted],
              :methods => :company_name
            }}
          }}
        })
      else
        render json: { status: :unprocessable_entity, error: "Unrecognized date format" }
      end
    else
      render json: { status: :unprocessable_entity, error: "Start date not found" }
    end
  end
end