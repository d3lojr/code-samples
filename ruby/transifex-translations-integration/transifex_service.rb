module TransifexService
  class << self
    def ignored_fields
      ['published','image','site_ids','site_urls','site_id','url','_tags']
    end

    def translate(model, tag, language, stripformat = false)
      translation = TransifexTranslation.find_by(cms_model: model, filename: tag, locale: language)

      unless translation.nil?
        if model.include? "Pages_Page"
          as_hash = eval(translation[:content])
        else
          if stripformat
            translation.content_stripformat
          else
            translation[:content].html_safe
          end
        end
      else
        nil
      end
    end

    def has_translations?(model, locale)
      translations = TransifexTranslation.where(
        cms_model: "#{model.class.name.deconstantize}_#{model.class.name.demodulize}_#{model.id}",
        locale: locale
      )

      translations.present?
    end

    def get_translations(model, locale)
      require 'rest-client'
      require 'algoliasearch'

      resource = RestClient::Resource.new("https://www.transifex.com/api/2/project/mirioncom-staging/resource/#{model.class.name.deconstantize}_#{model.class.name.demodulize}_#{model.id}/translation/#{locale}/?mode=reviewed&file", 'api', '1/1b16afc53cd5de9bde67c63930b6446c0481eeed')

      begin
        response = resource.get()

        if response.code == 200
          yaml = YAML.load(response.body)
          strings = yaml[locale]
          strings.each do |local_key, translated_content|
            # Create TransifexTranslation
            translation = TransifexTranslation.find_or_create_by(
              cms_model: "#{model.class.name.deconstantize}_#{model.class.name.demodulize}_#{model.id}",
              filename: local_key,
              locale: locale
            )
            if model.class == ::Forms::Form && local_key[-8..-1] == '_options'
              translated_content.gsub!('<br />', "\r\n")
            end
            translation.update(content: translated_content)
          end

          # push translation to algolia
          if model.respond_to?(:index_id)
            Algolia.init(application_id: ENV['ALGOLIASEARCH_APPLICATION_ID'], api_key: ENV['ALGOLIASEARCH_API_KEY'])
            master_index = Algolia::Index.new(Products::Product.index_name)

            # lookup what attributes we have indexed for this model
            # TODO: is it safe to assume that we will always have this object in the index?
            master_attributes = master_index.get_objects([model.index_id]).first

            algolia_record = {}
            # reuse any algolia attributes that are non-language specific
            master_attributes.keys.select{ |k| ignored_fields.include?(k) }.each do |k|
              algolia_record[k] = master_attributes[k]
            end

            sections = []

            strings.each do |local_key, translated_content|
              # if there's a simple 1-1 correspondence of attributes from algolia's attributes to transifex's attributes, 
              # add the translated string to the algolia record
              if master_attributes.keys.include?(local_key.to_s)
                algolia_record[local_key.to_s] = translated_content
              end

              if translated_content.is_a?(Hash)
                # for pages, add all the translated content to "sections"
                # {:title=>"", 3420=>{"headline"=>"", "subheadline"=>""}, 3421=>{"headline"=>"", "subheadline"=>"", "container_id"=>""}, 3422=>{"headline"=>"", "cta_text"=>""}, 3423=>{"headline"=>""}, 3424=>{"headline"=>"", "subheadline"=>"", "container_id"=>""}, 3425=>{"headline"=>"", "subheadline"=>"", "container_id"=>""}, 3426=>{"headline"=>"", "subheadline"=>"", "container_id"=>""}, 3427=>{"article_1"=>"", "article_2"=>"", "article_3"=>"", "headline"=>"", "content"=>"", "link_cta"=>""}, 3428=>{"event_1"=>"", "event_2"=>"", "tradeshow_2"=>"", "headline"=>"", "link_cta"=>""}, 3429=>{"link_1"=>""}}
                translated_content.each do |k, sub_content|
                  sections << sub_content
                end
              else
                # look up any mapped fields (ie "teaser_text" is mapped to "search_description" for Articles::Article)
                # this algolia_process_field method needs to be defined for each model
                begin
                  algolia_record = algolia_record.merge(model.algolia_process_field(local_key, translated_content))
                rescue Exception => e
                  p "Error for #{local_key} (1)"
                  puts e
                end

                # services and divisions have multiple sections of content, add all the translated content to "sections"
                # {:section_5_title=>"", :section_5_content=>"", :section_6_title=>"", :section_6_content=>""}
                if local_key.to_s.match(/section_\d*_content/)
                  sections << translated_content
                end
              end
            end

            # special handling for product "search title"
            if model.class.name == "Products::Product" && master_attributes.key?('search_title') && algolia_record['search_title'].blank?
              algolia_record['search_title'] = "#{strings['model']} #{strings['title']}"
            end

            # special handling for models with "sections" (pages, services, divisions)
            unless sections.blank?
              algolia_record = algolia_record.merge(model.algolia_process_field('sections', sections.join(' ')))
            end

            # are there any other attributes that are translateable but not mapped (ie search_cta, type)
            orphan_attributes = master_attributes.keys.reject{ |k| algolia_record.keys.include?(k) }

            # try calling each method with the locale
            orphan_attributes.each do |k|
              begin
                algolia_record[k] = model.send(k, locale)
              rescue Exception => e
                p "Error for #{k} (2)"
                puts e
              end
            end

            # add/update this object in the localized index
            localized_index = Algolia::Index.new("#{Products::Product.index_name}_#{locale}")
            localized_index.add_object(algolia_record, model.index_id)
          end
        end
      rescue RestClient::ExceptionWithResponse => e
        raise e
      end
    end

    def create_translation(model)
      if model.class == ::Pages::Page
        if model.revision == true
          model = ::Pages::Page.unscoped.find(model.revision_master_id)
        end
      end

      if self.update_translations_file(model)
        require 'rest-client'

        resource = RestClient::Resource.new('https://www.transifex.com/api/2/project/mirioncom-staging/resources/', 'api', '1/1b16afc53cd5de9bde67c63930b6446c0481eeed')
        
        begin
          if model.class == ::Pages::Page
            identifier = "Page: #{model.title} (#{model.url(1)})"
          else
            identifier = model.respond_to?(:title) ? "#{model.class.name.demodulize}: #{model.title}" : "#{model.class.name.deconstantize}_#{model.class.name.demodulize}_#{model.id}"
          end

          slug = "#{model.class.name.deconstantize}_#{model.class.name.demodulize}_#{model.id}"

          unless identifier.blank?
            filepath = Rails.root.join('db', 'data', model.class.name.parameterize.gsub('-','_'), model.id.to_s + '.yml')
            response = resource.post({'i18n_type': 'YML', 'slug': slug, 'name': identifier, 'content': File.open(filepath, 'rb')})
          end
        rescue RestClient::ExceptionWithResponse => e
          raise e
        end
      end
    end

    def update_translation(model)
      if model.class == ::Pages::Page
        if model.revision == true
          model = ::Pages::Page.unscoped.find(model.revision_master_id)
        end
      end

      if self.update_translations_file(model)
        old_translations = TransifexTranslation.where(cms_model: "#{model.class.name.deconstantize}_#{model.class.name.demodulize}_#{model.id}")
        old_translations.destroy_all if old_translations.present?

        require 'rest-client'

        resource = RestClient::Resource.new("https://www.transifex.com/api/2/project/mirioncom-staging/resource/#{model.class.name.deconstantize}_#{model.class.name.demodulize}_#{model.id}/content", 'api', '1/1b16afc53cd5de9bde67c63930b6446c0481eeed')

        begin
          filepath = Rails.root.join('db', 'data', model.class.name.parameterize.gsub('-','_'), model.id.to_s + '.yml')
          response = resource.put({'i18n_type': 'YML', 'content': File.open(filepath, 'rb')})
        rescue RestClient::ResourceNotFound => e
          create_translation(model)
        rescue RestClient::ExceptionWithResponse => e
          raise e
        end
      end
    end

    def update_translations_file(model)
      if model.respond_to?('as_transifex_json')
        require 'nokogiri'
        require 'fileutils'

        path = Rails.root.join('db', 'data', model.class.name.parameterize.gsub('-','_'))
        unless File.directory?(path)
          FileUtils.mkdir_p(path)
        end

        filepath = Rails.root.join('db', 'data', model.class.name.parameterize.gsub('-','_'), model.id.to_s + '.yml')
        File.open(filepath, "w+") do |f|
          f.write({'en' => model.as_transifex_json}.to_yaml(indentation: 4))
        end
      else
        return false
      end
    end
  end
end
