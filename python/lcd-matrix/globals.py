import time

# LED strip configuration:
LED_COUNT      = 256     # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 1       # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

# Define functions which animate LEDs in various ways.
def colorWipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(wait_ms/1000.0)

def moveLeft(pins):
    i = 0
    while i < len(pins):
        pins[i] = pins[i] - (((pins[i] % 8)*2)+1)
        if pins[i] < 0:
            pins[i] += 256
        i += 1

def moveRight(pins):
    i = 0
    while i < len(pins):
        pins[i] = pins[i] + (((8 - (pins[i] % 8)) * 2) - 1)
        if pins[i] > 256:
            pins[i] -= 256
        i += 1

def whiteout(strip, wait_ms=20, iterations=1):
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, Color(255,255,255))
    strip.show()
    time.sleep(wait_ms/1000.0)
