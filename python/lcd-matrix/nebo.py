#!/usr/bin/env python3

import time
from neopixel import *
import argparse

import globals

# Main program logic follows:
if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(globals.LED_COUNT, globals.LED_PIN, globals.LED_FREQ_HZ, globals.LED_DMA, globals.LED_INVERT, globals.LED_BRIGHTNESS, globals.LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:
	nebo = [
	    # n
	    9,10,13,17,18,19,20,21,22,26,27,28,29,30,34,
	    45,46,49,50,51,52,53,54,57,58,59,60,61,
	    # e
	    74,75,76,77,81,82,83,84,85,86,89,92,94,
	    97,98,99,101,102,106,109,
	    # b
	    121,122,123,124,125,126,131,132,133,134,
	    137,141,146,147,148,149,150,
	    # o
	    162,163,164,165,169,170,171,172,173,174,
	    177,182,185,186,187,188,189,190,
	    194,195,196,197
	]
	while True:
	    i = 0
	    while i < len(nebo):
	    	strip.setPixelColor(nebo[i], Color(110,110,255))
		i += 1
	    strip.show()
	    i = 0
	    while i < len(nebo):
                strip.setPixelColor(nebo[i], Color(0,0,0))
                i += 1
	    globals.moveLeft(nebo)
	    time.sleep(0.1)

    except KeyboardInterrupt:
        if args.clear:
            globals.colorWipe(strip, Color(0,0,0), 10)

