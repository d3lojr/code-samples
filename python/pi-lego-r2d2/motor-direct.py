import RPi.GPIO as GPIO
import time
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-r", help="reverse motor", dest="reverse", action="store_true")

(options, args) = parser.parse_args()

GPIO.setmode(GPIO.BOARD)

# Data Output Pins
# One per coil, in each cardinal direction
ControlPin = [35,37,38,40]

# Setup pins for Output Mode
# Set all pins to low/off
for pin in ControlPin:
	GPIO.setup(pin,GPIO.OUT)
	GPIO.output(pin,0)

# Halfstep rotation sequence
seq = [ [1,0,0,0],
	[1,1,0,0],
	[0,1,0,0],
	[0,1,1,0],
	[0,0,1,0],
	[0,0,1,1],
	[0,0,0,1],
	[1,0,0,1] ]

# Uncomment this line to reverse direction of motor
if options.reverse:
	seq = list(reversed(seq))

for i in range(512):
	for halfstep in range(8):
		for pin in range(4):
			GPIO.output(ControlPin[pin],seq[halfstep][pin])
		time.sleep(0.00089)

GPIO.cleanup()
