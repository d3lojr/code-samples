import RPi.GPIO as GPIO
# GPIO.setmode(GPIO.BCM)
import sys, tty, termios, time

# Get rid of warnings
GPIO.setwarnings(False)

# Set the GPIO mode
GPIO.setmode(GPIO.BOARD)

# Set the pins to be outputs
GPIO.setup(11,GPIO.OUT)
GPIO.setup(13,GPIO.OUT)
GPIO.setup(15,GPIO.OUT)
GPIO.setup(16,GPIO.OUT)

# Old Code (this sucks)
# 
#A routine to control a pair of pins
def ControlAPairOfPins(FirstPin,FirstState,SecondPin,SecondState):
  if FirstState == "1":
    GPIO.output(int(FirstPin),True)
  else:
    GPIO.output(int(FirstPin),False)

  if SecondState == "1":
    GPIO.output(int(SecondPin),True)
  else:
    GPIO.output(int(SecondPin),False)
  return

# while True:
#   MyChar = raw_input("Press a character:")
#   print "You pressed: " + MyChar
#   if MyChar == "q":
#     ControlAPairOfPins("11","1","13","0")
#     ControlAPairOfPins("15","0","16","1")
#     print "Forward"
#   elif MyChar == "a":
#     ControlAPairOfPins("15","0","16","0")
#     ControlAPairOfPins("11","0","13","0")
#     print "Stop"
#   elif MyChar == "z":
#     ControlAPairOfPins("11","0","13","1")
#     ControlAPairOfPins("15","1","16","0")
#     print ("Back")
#   elif MyChar == "i":
#     ControlAPairOfPins("11","1","13","0")
#     ControlAPairOfPins("15","1","16","0")
#     print "Left"
#   elif MyChar == "o":
#     ControlAPairOfPins("15","0","16","0")
#     ControlAPairOfPins("11","0","13","0")
#     print "Stop steering"
#   elif MyChar == "p":
#     ControlAPairOfPins("11","0","13","1")
#     ControlAPairOfPins("15","0","16","1")
#     print "Right"
#   else:
#     print "Not a command"


# The getch method can determine which key has been pressed
# by the user on the keyboard by accessing the system files
# It will then return the pressed key as a variable
def getch():
  fd = sys.stdin.fileno()
  old_settings = termios.tcgetattr(fd)
  try:
    tty.setraw(sys.stdin.fileno())
    ch = sys.stdin.read(1)
  finally:
    termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
  return ch

# Infinite loop that will not end until the user presses "x"
# http://www.instructables.com/id/Controlling-a-Raspberry-Pi-RC-Car-With-a-Keyboard/
while True:
  # Keyboard character retrieval method
  char = getch()

  # The car will drive forward when the "w" key is pressed
  if(char == "w"):
    ControlAPairOfPins("11","1","13","0")
    ControlAPairOfPins("15","0","16","1")

  # The car will reverse when the "s" key is pressed
  if(char == "s"):
    ControlAPairOfPins("11","0","13","1")
    ControlAPairOfPins("15","1","16","0")

  # The "a" key will toggle the steering left
  if(char == "a"):
    ControlAPairOfPins("11","1","13","0")
    ControlAPairOfPins("15","1","16","0")

  # The "d" key will toggle the steering right
  if(char == "d"):
    ControlAPairOfPins("11","0","13","1")
    ControlAPairOfPins("15","0","16","1")

  # TODO: The "l" key will toggle the LEDs on/off
  # if(char == "l"):
  #     toggleLights()

  # The "x" key will break the loop and exit the program
  if(char == "e"):
    ControlAPairOfPins("15","0","16","0")
    ControlAPairOfPins("11","0","13","0")

  if(char == "x"):
    print("Program Ended")
    break

  # At the end of each loop the acceleration motor will stop
  # and wait for its next command
  # motor2.ChangeDutyCycle(0)

  # The keyboard character variable will be set to blank, ready
  # to save the next key that is pressed
  char = ""
