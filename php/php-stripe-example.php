<?php

// ... Code has been removed at request of the client

function make_donation($params){
	global $stripe_skey;

	if(!verifyRecaptcha($params['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'])) {
		$response['success'] = false;
		$response['error'] = "Please verify you're a human by completing the CAPTCHA";
	} else {
		extract($params);
		if(!preg_match('/\b\d{1,3}(?:,?\d{3})*(?:\.\d{2})?\b/', $donation['amount'])){
			$response['success'] = false;
			$response['error'] = 'Please enter a valid donation amount.';
			return $response;
		}
	
		$donation['amount'] = preg_replace("/[^0-9.]/",'',$donation['amount']);

		// Stripe Method
		try {
			\Stripe\Stripe::setApiKey($stripe_skey);
			// \Stripe\Stripe::$apiBase = "https://api-tls12.stripe.com";

			$charge = \Stripe\Charge::create(array(
			  "amount" => (((float)$donation['amount'])*100),
			  "currency" => "usd",
			  "source" => $stripeToken,
			  "receipt_email" => $billing['email'],
			  "description" => $billing['email']
			));

			if(empty($donation['on-behalf-of'])) {
				send_receipt($billing['first-name'].' '.$billing['last-name'], $billing['email'],'Donation (' . $donation['reason'] . ')',$donation['amount']);
			} else {
				send_receipt($billing['first-name'].' '.$billing['last-name'].' (on behalf of: '.$donation['on-behalf-of'].')', $billing['email'],'Donation (' . $donation['reason'] . ')',$donation['amount']);
			}
			save_donation($params);
			$response['success'] = true;
		} catch (\Stripe\Error\Card $e) {
			$response['success'] = false;
			$response['error'] = $e->getMessage();
		}
	}
	return $response;
}
function make_archives_donation($params) {
	global $db;

	if(!verifyRecaptcha($params['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'])) {
		$response['success'] = false;
		$response['error'] = "Please verify you're a human by completing the CAPTCHA";
	} else {
		extract($params);

		$values = array('first_name' => $personal['first-name'],
			'last_name' => $personal['last-name'],
			'email' => $personal['email'],
			'phone' => $personal['phone'],
			'comments' => $donation['description'],
			'date' => date("Y-m-d H:i:s", time())
		);
		$db->insert('archives_donations',$values);

		send_archives_donation_email($params);

		$response['success'] = true;
	}
	return $response;
}
function request_a_fine($params) {
	global $db;

	if(!verifyRecaptcha($params['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'])) {
		$response['success'] = false;
		$response['error'] = "Please verify you're a human by completing the CAPTCHA";
	} else {
		extract($params);

		$values = array('first_name' => $personal['first-name'],
			'last_name' => $personal['last-name'],
			'email' => $personal['campus-email-address'],
			'institution' => $personal['institution-name'],
			'student_faculty' => $personal['student-faculty-member'],
			'library_barcode' => $personal['library-barcode-number'],
			'fine_or_lost' => $personal['fine-or-lost-book'],
			'comments' => $additional['description'],
			'date' => date("Y-m-d H:i:s", time())
		);
		$db->insert('fine_requests',$values);

		send_fine_request_email($params);

		$response['success'] = true;
	}
	return $response;
}
function pay_a_fine($params) {
	global $db, $stripe_skey;

	if(!verifyRecaptcha($params['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'])) {
		$response['success'] = false;
		$response['error'] = "Please verify you're a human by completing the CAPTCHA";
	} else {
		extract($params);
		if(!preg_match('/\b\d{1,3}(?:,?\d{3})*(?:\.\d{2})?\b/', $fine['amount'])){
			$response['success'] = false;
			$response['error'] = 'Please enter a valid fine amount.';
			return $response;
		}

		// Stripe Method
		try {
			\Stripe\Stripe::setApiKey($stripe_skey);

			$fine['amount'] = preg_replace("/[^0-9.]/",'',$fine['amount']);

			$charge = \Stripe\Charge::create(array(
			  "amount" => (((float)$fine['amount'])*100),
			  "currency" => "usd",
			  "source" => $stripeToken,
			  "receipt_email" => $billing['email']
			));

			$values = array('first_name' => $personal['first-name'],
				'last_name' => $personal['last-name'],
				'email' => $personal['campus-email-address'],
				'institution' => $personal['institution-name'],
				'student_faculty' => $personal['student-faculty-member'],
				'library_barcode' => $personal['library-barcode-number'],
				'fine_or_lost' => $personal['fine-or-lost-book'],
				'amount' => $fine['amount'],
				'billing_first_name' => $billing['first-name'],
				'billing_last_name' => $billing['last-name'],
				'address_1' => $billing['address-1'],
				'address_2' => $billing['address-2'],
				'city' => $billing['city'],
				'state' => $billing['state'],
				'zipcode' => $billing['zipcode'],
				'receipt_email' => $billing['email'],
				'date' => date("Y-m-d H:i:s", time())
			);
			$db_result = $db->insert('fine_payments',$values);

			send_fine_payment_email($params);

			$response['success'] = true;
		} catch (\Stripe\Error\Card $e) {
			$response['success'] = false;
			$response['error'] = $e->getMessage();
		}
	}
	return $response;
}
function pay_a_service($params) {
	global $db, $stripe_skey;

	if(!verifyRecaptcha($params['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'])) {
		$response['success'] = false;
		$response['error'] = "Please verify you're a human by completing the CAPTCHA";
	} else {
		extract($params);
		if(!preg_match('/\b\d{1,3}(?:,?\d{3})*(?:\.\d{2})?\b/', $payment['amount'])){
			$response['success'] = false;
			$response['error'] = 'Please enter a valid payment amount.';
			return $response;
		}

		// Stripe Method
		try {
			\Stripe\Stripe::setApiKey($stripe_skey);

			$charge = \Stripe\Charge::create(array(
			  "amount" => (((float)$payment['amount'])*100),
			  "currency" => "usd",
			  "source" => $stripeToken,
			  "receipt_email" => $billing['email']
			));

			$values = array('service' => $service['name'],
				'first_name' => $personal['first-name'],
				'last_name' => $personal['last-name'],
				'email' => $personal['email'],
				'phone' => $personal['phone-number'],
				'researcher' => $personal['researcher-name'],
				'primary_contact_name' => $primary['name'],
				'primary_contact_email' => $primary['email'],
				'primary_contact_phone' => $primary['phone-number'],
				'event_name' => $primary['event-name'],
				'room_number' => $primary['room-number'],
				'event_date' => $primary['event-date'],
				'event_time' => $primary['event-time'],
				'from_date' => $primary['from-date'],
				'to_date' => $primary['to-date'],
				'study_carrel_number' => $primary['study-carrel-number'],
				'num_participants' => $primary['participant-count'],
				'num_hours' => $primary['number-of-hours'],
				'num_shuttles' => $primary['number-of-shuttles'],
				'institution' => $primary['institution-name'],
				'organization' => $primary['organization-name'],
				'amount' => $payment['amount'],
				'billing_first_name' => $billing['first-name'],
				'billing_last_name' => $billing['last-name'],
				'address_1' => $billing['address-1'],
				'address_2' => $billing['address-2'],
				'city' => $billing['city'],
				'state' => $billing['state'],
				'zipcode' => $billing['zipcode'],
				'receipt_email' => $billing['email'],
				'date' => date("Y-m-d H:i:s", time())
			);
			$db_result = $db->insert('services_payments',$values);

			send_services_notification_email($params);

			$response['success'] = true;
		} catch (\Stripe\Error\Card $e) {
			$response['success'] = false;
			$response['error'] = $e->getMessage();
		}
	}
	return $response;
}
function create_member($params){
	global $db, $stripe_skey;

	if(!verifyRecaptcha($params['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'])) {
		$response['success'] = false;
		$response['error'] = "Please verify you're a human by completing the CAPTCHA";
	} else {
		extract($params);
		if(!intval($membership['type'])){
			$response['success'] = false;
			$response['error'] = 'Please select a membership type.';
		}
		if(isValidEmail($membership['email'])){
			$membership_id = intval($membership['type']);
			$membership_type = $db->getRow("SELECT * FROM membership_types WHERE id = {$membership_id}");

			// Stripe Method
			try {
				\Stripe\Stripe::setApiKey($stripe_skey);

				$charge = \Stripe\Charge::create(array(
				  "amount" => (((float)$membership_type['price'])*100),
				  "currency" => "usd",
				  "source" => $stripeToken,
				  "receipt_email" => $billing['email'],
				  "description" => $billing['email']
				));

				$values = array('first_name' => $membership['first-name'],
								'last_name' => $membership['last-name'],
								'email' => $membership['email'],
								'address_1' => $membership['address-1'],
								'address_2' => $membership['address-2'],
								'city' => $membership['city'],
								'state' => $membership['state'],
								'zipcode' => $membership['zipcode'],
								'type' => $membership['type'],
								'affiliation' => $membership['affiliation'],
								'published_at' => date("Y-m-d H:i:s", time())
				);
				if($membership['affiliation'] == 'Other')
					$values['affiliation'] = $membership['other_affiliation'];
				$response['success'] = true;
				$response['member_id'] = $db->insert('members',$values);
				send_receipt($billing['first-name'].' '.$billing['last-name'], $membership['email'],'Membership Dues (' . $membership_type['title'] . ')',$membership_type['price']);
			} catch (\Stripe\Error\Card $e) {
				$response['success'] = false;
				$response['error'] = $e->getMessage();
			}
		}else{
			$response['success'] = false;
			$response['error'] = 'Please enter a valid email address.';
		}
	}
	return $response;
}

function verifyRecaptcha($recaptchaResponse, $userIP) {
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$fields = array(
		'secret' => urlencode('//KEY REMOVED FROM SOURCE CODE//'),
		'response' => urlencode($recaptchaResponse),
		'remoteip' => urlencode($userIP)
	);
	$fields_string = '';
	foreach($fields as $key=>$value) {
		$fields_string .= $key.'='.$value.'&';
	}
	rtrim($fields_string, '&');

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, count($fields));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);

	$result = json_decode(curl_exec($ch));
	curl_close($ch);

	return ($result->success == true);
}

// ... Code has been removed at request of the client